//ProjectEulerRust/src/Problems/Problems7.rs
//Matthew Ellison
// Created: 06-15-20
//Modified: 07-21-20
//What is the 10001th prime number?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"What is the 10001th prime number?".to_string()
}

pub fn solve() -> Answer{
	let NUMBER_OF_PRIMES = num::BigInt::from(10001);
	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Get all the prime numbers
	let primes = myClasses::Algorithms::getNumPrimesBig(num::BigInt::new(NUMBER_OF_PRIMES.sign(), NUMBER_OF_PRIMES.to_u32_digits().1));

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The {}th prime number is {}", NUMBER_OF_PRIMES, primes[primes.len() - 1]), timer.getString(), timer.getNano());
}

/* Results:
The 10001th prime number is 104743
It took an average of 101.440 milliseconds to run this problem through 100 iterations
*/
