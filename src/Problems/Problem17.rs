//ProjectEulerRust/src/Problems/Problems17.rs
//Matthew Ellison
// Created: 06-16-20
//Modified: 07-21-20
//If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#![allow(unused_assignments)]

extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?".to_string()
}

pub fn solve() -> Answer{
	let START_NUM = 1;
	let STOP_NUM = 1000;
	let mut sumOfLetters = 0;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Start with 1 and increment
	for num in START_NUM..=STOP_NUM{
		//Pass the number to a function that will create a string for the number
		let currentNumString = getStringFromNum(num);
		//Pass the string to the function that will count the number of letters in it, ignoring whitespace and punctuation and add that number to the running tally
		sumOfLetters += getNumberChars(currentNumString);
	}

	//Return the results
	return Answer::new(format!("The sum of all the letters in all the number {}-{} is {}", START_NUM, STOP_NUM, sumOfLetters), timer.getString(), timer.getNano());
}

fn getStringFromNum(orgNumber: i32) -> String{
	let mut number = orgNumber;
	let mut numberString = "".to_string();
	//Starting with the largest digit create a string based on the number passed in
	//Check for negative
	if(number < 0){
		numberString.push_str("negative");
	}

	//Check if the number is zero
	if(number == 0){
		numberString.push_str("zero");
	}

	//Start with the thousands place
	if((number / 1000) >= 1){
		numberString.push_str(getStringFromNum(number / 1000).as_str());
		numberString.push_str(" thousand");
		number -= (number / 1000) * 1000;
	}

	//Check the hundreds place
	if((number / 100) >= 1){
		numberString.push_str(getStringFromNum(number / 1000).as_str());
		numberString.push_str(" hundred");
		number -= (number / 100) * 100;
	}

	//Insert an and if there is need
	if((!numberString.is_empty()) && (number > 0)){
		numberString.push_str(" and ");
	}

	//Check for tens place
	if((number / 10) >= 2){
		//For the tens you need to do something special
		let tensPlace = (number / 10);
		if(tensPlace == 9){
			numberString.push_str("ninety");
		}
		else if(tensPlace == 8){
			numberString.push_str("eighty");
		}
		else if(tensPlace == 7){
			numberString.push_str("seventy");
		}
		else if(tensPlace == 6){
			numberString.push_str("sixty");
		}
		else if(tensPlace == 5){
			numberString.push_str("fifty");
		}
		else if(tensPlace == 4){
			numberString.push_str("forty");
		}
		else if(tensPlace == 3){
			numberString.push_str("thirty");
		}
		else if(tensPlace == 2){
			numberString.push_str("twenty");
		}
		number -= (tensPlace * 10);
		//If there is something left in the number you will need a dash to separate the tens and ones place
		if(number > 0){
			numberString.push_str("-");
		}
	}
	//Check for teens
	else if((number / 10) >= 1){
		let onesPlace = number % 10;
		if(onesPlace == 9){
			numberString.push_str("nineteen");
		}
		else if(onesPlace == 8){
			numberString.push_str("eighteen");
		}
		else if(onesPlace == 7){
			numberString.push_str("seventeen");
		}
		else if(onesPlace == 6){
			numberString.push_str("sixteen");
		}
		else if(onesPlace == 5){
			numberString.push_str("fifteen");
		}
		else if(onesPlace == 4){
			numberString.push_str("fourteen");
		}
		else if(onesPlace == 3){
			numberString.push_str("thirteen");
		}
		else if(onesPlace == 2){
			numberString.push_str("twelve");
		}
		else if(onesPlace == 1){
			numberString.push_str("eleven");
		}
		else if(onesPlace == 0){
			numberString.push_str("ten");
		}
		//If this was hit the number was completed
		number = 0;
	}

	//Check for the ones place
	if(number >= 1){
		if(number == 9){
			numberString.push_str("nine");
		}
		else if(number == 8){
			numberString.push_str("eight");
		}
		else if(number == 7){
			numberString.push_str("seven");
		}
		else if(number == 6){
			numberString.push_str("six");
		}
		else if(number == 5){
			numberString.push_str("five");
		}
		else if(number == 4){
			numberString.push_str("four");
		}
		else if(number == 3){
			numberString.push_str("three");
		}
		else if(number == 2){
			numberString.push_str("two");
		}
		else if(number == 1){
			numberString.push_str("one");
		}
		//If this was hit the number was completed
		number = 0;
	}

	//Return the string
	return numberString;
}

fn getNumberChars(number: String) -> i32{
	let mut sumOfLetters = 0;
	//Count the number of letters, ignoring puctuation and whitespace
	for letter in number.chars(){
		if(letter.is_alphabetic()){
			sumOfLetters += 1;
		}
	}

	//Return the number of letters
	return sumOfLetters;
}

/* Results:
The sum of all the letters in all the number 1-1000 is 21124
It took an average of 356.809 microseconds to run this problem through 100 iterations
*/
