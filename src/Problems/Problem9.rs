//ProjectEulerRust/src/Problems/Problems9.rs
//Matthew Ellison
// Created: 06-15-20
//Modified: 07-21-20
//There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.".to_string()
}

pub fn solve() -> Answer{
	let mut a = 1.0;	//Holds the length of the first side
	let mut b = 0.0;	//Holds the length of the second side
	let mut c = 0.0;//Holds the length of the hyp
	let mut found = false;	//A flag to show whether the solution has been found

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Loop through all possible a's
	while((a < 1000.0) && !found){
		b = a + 1.0;	//b must be larget than a
		c = (((a * a) + (b * b)) as f64).sqrt();

		//Loop through all possible b's for this a
		while((a + b + c) < 1000.0){
			b += 1.0;
			c = (((a * a) + (b * b)) as f64).sqrt();
		}

		//If the sum is 1000 you found the number, otherwise go to the next possible a
		if(a + b + c == 1000.0){
			found = true;
		}
		else{
			a += 1.0;
		}
	}

	//Stop the timer
	timer.stop();

	//Save the results
	if(found){
		return Answer::new(format!("The Pythagorean triplet is {} + {} + {}\nThe numbers' product is {}", a, b, c, a * b * c), timer.getString(), timer.getNano());
	}
	else{
		return Answer::new(format!("The number was not found!"), timer.getString(), timer.getNano());
	}
}

/* Result:
The Pythagorean triplet is 200 + 375 + 425
The numbers' product is 31875000
It took an average of 147.890 microseconds to run this problem through 100 iterations
*/
