//ProjectEulerRust/src/Problems/Problems14.rs
//Matthew Ellison
// Created: 06-16-20
//Modified: 07-21-20
//Work out the first ten digits of the sum of the following one-hundred 50-digit numbers
/*
The following iterative sequence is defined for the set of positive integers:
n → n/2 (n is even)
n → 3n + 1 (n is odd)
Which starting number, under one million, produces the longest chain?
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"Which starting number, under one million, produces the longest chain using the itterative sequence?".to_string()
}

pub fn solve() -> Answer{
	let MAX_NUM = 1_000_000_i64;
	//This is the length of the longest chain
	let mut maxLength = 0i64;
	//This is the starting number of the longest chain
	let mut maxNum = 0i64;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Loop through all numbers less than MAX_NUM and check them against the series
	for currentNum in 1i64..MAX_NUM{
		let currentLength = checkSeries(currentNum);
		//If the current number has a longer series than the max then the current becomes the max
		if(currentLength > maxLength){
			maxLength = currentLength;
			maxNum = currentNum;
		}
	}

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The number {} produced a chain of {} steps", maxNum, maxLength), timer.getString(), timer.getNano());
}

fn checkSeries(startNum: i64) -> i64{
	let mut num = startNum;
	//Start at 1 because you need to count the starting number
	let mut length = 1i64;

	//Follow the series, adding 1 for each step you take
	while(num > 1){
		if((num % 2) == 0){
			num /= 2;
		}
		else{
			num = (3 * num) + 1;
		}
		length += 1;
	}

	//Return the length of the series
	return length;
}

/* Results:
The number 837799 produced a chain of 525 steps
It took an average of 99.323 milliseconds to run this problem through 100 iterations
*/
