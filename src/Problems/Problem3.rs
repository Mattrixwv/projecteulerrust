//ProjectEulerRust/src/Problems/Problems3.rs
//Matthew Ellison
// Created: 06-13-20
//Modified: 07-21-20
//The largest prime factor of 600851475143
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

extern crate num;


pub fn getDescription() -> String{
	"What is the largest prime factor of 600851475143?".to_string()
}

pub fn solve() -> Answer{
	//Setup the number
	let number = "600851475143".parse::<num::BigInt>().unwrap();
	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Get all the factors of the number
	let factors = myClasses::Algorithms::getFactorsBig(num::BigInt::new(number.sign(), number.to_u32_digits().1));
	//The last element should be the largest factor

	//Stop the timer
	timer.stop();

	//Save the results
	return Answer::new(format!("The largest factor of the number {} is {}", number, factors.last().unwrap()), timer.getString(), timer.getNano());
}

/* Results:
The largest factor of the number 600851475143 is 6857
It took an average of 1.272 seconds to run this problem through 100 iterations
*/
