//ProjectEulerRust/src/Problems/Problems10.rs
//Matthew Ellison
// Created: 06-15-20
//Modified: 07-21-20
//Find the sum of all the primes below two million
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"Find the sum of all the primes below two million".to_string()
}

pub fn solve() -> Answer{
	let GOAL_NUMBER = 2_000_000;
	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Get the sum of all prime numbers < GOAL_NUMBER
	let sum: i64 = myClasses::Algorithms::getPrimes(GOAL_NUMBER - 1).iter().sum();

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The sum of all the primes < {} is {}", GOAL_NUMBER, sum), timer.getString(), timer.getNano());
}

/* Results:
The sum of all the primes < 2000000 is 142913828922
It took an average of 160.511 milliseconds to run this problem through 100 iterations
*/
