//ProjectEulerRust/src/Problems/Problems21.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//Evaluate the sum of all the amicable numbers under 10000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"Evaluate the sum of all the amicable numbers under 10000".to_string()
}

pub fn solve() -> Answer{
	let LIMIT = 10000;
	//Setup the variables
	let mut divisorSum = Vec::<i64>::new();
	//Make sure the array is filled with 0's
	while(divisorSum.len() < LIMIT){
		divisorSum.push(0);
	}

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Generate the divisors of all numbers < 10000, get their sum, and add it to the list
	for cnt in 1..LIMIT{
		let mut divisors = myClasses::Algorithms::getDivisors(cnt as i64);
		if(divisors.len() > 1){
			divisors.remove(divisors.len() - 1);
		}
		divisorSum[cnt] = divisors.iter().sum();
	}
	//Check every sum of divisors in the list for matching sum
	let mut amicable = Vec::<i64>::new();
	for cnt in 1..divisorSum.len(){
		let sum = divisorSum[cnt];
		//If the sum is greater than the number of divisors then it is impossible to be amicable. Skip the number and continue
		if(sum >= divisorSum.len() as i64){
			continue;
		}
		//We know that divisorSum.at(cnt) == sum, so if divisorSum.at(sum) == cnt we found an amicable number
		if(divisorSum[sum as usize] == cnt as i64){
			//A number can't be amicable with itself
			if(sum == cnt as i64){
				continue;
			}
			//Add it to the array of amicable numbers
			amicable.push(cnt as i64);
		}
	}

	//Stop the timer
	timer.stop();

	//Sort the array for neatness
	amicable.sort();

	//Return the results
	let mut answerString = format!("All amicable numbers less than {} are\n", LIMIT);
	for cnt in 0..amicable.len(){
		answerString = format!("{}{}\n", answerString, amicable[cnt]);
	}
	answerString = format!("{}The sum of all of these amicable numbers is {}", answerString, amicable.iter().sum::<i64>());
	return Answer::new(answerString, timer.getString(), timer.getNano());
}

/* Results:
All amicable numbers less than 10000 are
220
284
1184
1210
2620
2924
5020
5564
6232
6368
The sum of all of these amicable numbers is 31626
It took an average of 5.920 milliseconds to run this problem through 100 iterations
*/
