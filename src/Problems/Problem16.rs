//ProjectEulerRust/src/Problems/Problems16.rs
//Matthew Ellison
// Created: 06-16-20
//Modified: 07-21-20
//What is the sum of the digits of the number 2^1000?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"What is the sum of the digits of the number 2^1000?".to_string()
}

pub fn solve() -> Answer{
	//The number that is going to be raised to a power
	let NUM_TO_POWER = 2;
	//The pwer that the number is going to be raised to
	let POWER = 1000;
	//The number to be calculated
	let mut sumOfElements = 0u64;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Get the number
	let number = num::pow::pow(num::BigInt::from(NUM_TO_POWER), POWER);

	//Get a string of the number
	let numString = number.to_str_radix(10);

	//Add up the individual characters of the string
	for n in numString.chars(){
		sumOfElements += n.to_digit(10).unwrap() as u64;
	}

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("{}^{} = {}\nThe sum of the elements is {}", NUM_TO_POWER, POWER, number, sumOfElements), timer.getString(), timer.getNano());
}

/* Results:
2^1000 = 10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376
The sum of the elements is 1366
It took an average of 6.483 microseconds to run this problem through 100 iterations
*/
