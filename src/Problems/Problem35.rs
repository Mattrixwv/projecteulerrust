//ProjectEulerRust/src/Problems/Problem35.rs
//Matthew Ellison
// Created: 06-06-21
//Modified: 06-06-21
//How many circular primes are there below one million?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"How many circular primes are there below one million?".to_string()
}

pub fn solve() -> Answer{
	//Variables
	//Static variables
	let maxNum = 999999;	//The largest number that we are checking for primes
	//Instance variables
	let mut circularPrimes = Vec::<i64>::new();

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Get all primes under 1,000,000
	let primes = myClasses::Algorithms::getPrimes(maxNum);
	//Go through all primes, get all their rotations, and check if those numbers are also primes
	for num in &primes{
		let prime = *num;
		let mut allRotationsPrime = true;
		//Get all of the rotations of the prime and see if they are also prime
		let rotations = getRotations(&prime.to_string());
		for rotation in rotations{
			let p = rotation.parse::<i64>().unwrap();
			if(!primes.contains(&p)){
				allRotationsPrime = false;
				break;
			}
		}
		//If all rotations are prime add it to the list of circular primes
		if(allRotationsPrime){
			circularPrimes.push(prime);
		}
	}

	//Stop the timer
	timer.stop();

	return Answer::new(format!("The number of all circular prime numbers under {} is {}", maxNum, circularPrimes.len()), timer.getString(), timer.getNano());
}

fn getRotations(par1: &String) -> Vec::<String>{
	let mut string = String::from(par1);
	let stringCount = String::from(par1).chars().count();
	let mut rotations = Vec::<String>::new();
	rotations.push(string.to_string());
	for _cnt in 1 as usize .. stringCount{
		let mut tempString = string[1..].to_string();
		let tempString2 = string[..1].to_owned();
		tempString.push_str(&tempString2);
		string = String::from(tempString);
		rotations.push(string.to_string());
	}
	return rotations;
}

/* Results:
The number of all circular prime numbers under 999999 is 55
It took an average of 3.576 seconds to run this problem through 100 iterations
*/
