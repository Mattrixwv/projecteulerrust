//ProjectEulerRust/src/Problems/Problems33.rs
//Matthew Ellison
// Created: 02-07-21
//Modified: 02-07-21
/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s
We shall consider fractions like, 30/50 = 3/5, to be trivial examples
There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator
If the product of these four fractions is given in its lowest common terms, find the value of the denominator
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"If the product of these four fractions is given in its lowest common terms, find the value of the denominator".to_string()
}

pub fn solve() -> Answer{
	let mut numerators = Vec::<i32>::new();
	let mut denominators = Vec::<i32>::new();

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	for denominator in 11..=99{
		for numerator in 10..denominator{
			let num1 = numerator.to_string();
			let denom = denominator.to_string();
			let mut tempNum = 0;
			let mut tempDenom = 1;

			//Check that this isn't a trivial example
			if((num1.chars().nth(1).unwrap() == '0') && (denom.chars().nth(1).unwrap() == '0')){
				continue;
			}
			//Remove the offending digits if they exist
			else if(num1.chars().nth(0).unwrap() == denom.chars().nth(0).unwrap()){
				tempNum = num1.chars().nth(1).unwrap() as i32 - 48;
				tempDenom = denom.chars().nth(1).unwrap() as i32 - 48;
			}
			else if(num1.chars().nth(0).unwrap() == denom.chars().nth(1).unwrap()){
				tempNum = num1.chars().nth(1).unwrap() as i32 - 48;
				tempDenom = denom.chars().nth(0).unwrap() as i32 - 48;
			}
			else if(num1.chars().nth(1).unwrap() == denom.chars().nth(0).unwrap()){
				tempNum = num1.chars().nth(0).unwrap() as i32 - 48;
				tempDenom = denom.chars().nth(1).unwrap() as i32 - 48;
			}
			else if(num1.chars().nth(1).unwrap() == denom.chars().nth(1).unwrap()){
				tempNum = num1.chars().nth(0).unwrap() as i32 - 48;
				tempDenom = denom.chars().nth(0).unwrap() as i32 - 48;
			}

			//Test if the new fraction is the same as the old one
			if((tempNum as f64 / tempDenom as f64) == (numerator as f64 / denominator as f64)){
				numerators.push(numerator);
				denominators.push(denominator);
			}
		}
	}

	//Get the product of the numbers
	let numProd = numerators.iter().product::<i32>();
	let denomProd = denominators.iter().product::<i32>();
	//Get the gcd to reduce to lowest terms
	let gcd = myClasses::Algorithms::gcd(numProd, denomProd);
	//Save the denominator
	let prodDenominator = denomProd / gcd;

	//Stop the timer
	timer.stop();

	return Answer::new(format!("The denominator of the product is {}", prodDenominator), timer.getString(), timer.getNano());
}

/* Results:
The denominator of the product is 100
It took an average of 717.868 microseconds to run this problem through 100 iterations
*/
