//ProjectEulerRust/src/Problems/Problems28.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction?".to_string()
}

pub fn solve() -> Answer{
	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Setup the grid
	let grid = setupGrid();
	//Find the sum of the diagonals in the grid
	let sumOfDiagonals = findSum(grid);

	//Stop the timer
	timer.stop();

	//Return the answer
	return Answer::new(format!("The sum of the diagonals in the given grid is {}", sumOfDiagonals), timer.getString(), timer.getNano());
}
fn setupGrid() -> Vec::<Vec::<i64>>{
	let mut grid = Vec::<Vec::<i64>>::new();
	//Fill the grid with 0's
	for cnt in 0..1001{
		//Add a blank vector
		grid.push(Vec::<i64>::new());
		for _ in 0..1001{
			grid[cnt].push(0);
		}
	}

	let mut finalLocation = false;	//A flag to indicate if the final location to be filled has been reached
	//Set the number that is going to be put at each location
	let mut currentNum = 1;
	//Start with the middle location and set it correctly and advance the tracker to the next number
	let mut xLocation = 500;
	let mut yLocation = 500;
	grid[yLocation][xLocation] = currentNum;
	currentNum += 1;
	//Move right the first time
	xLocation += 1;
	//Move in a circular pattern until you reach the final location
	while(!finalLocation){
		//Move down until you reach a blank location on the left
		while(!(grid[yLocation][xLocation - 1] == 0)){
			grid[yLocation][xLocation] = currentNum;
			currentNum += 1;
			yLocation += 1;
		}

		//Move left until you reach a blank location above
		while(!(grid[yLocation - 1][xLocation] == 0)){
			grid[yLocation][xLocation] = currentNum;
			currentNum += 1;
			xLocation -= 1;
		}

		//Move up until you reach a blank location above
		while(!(grid[yLocation][xLocation + 1] == 0)){
			grid[yLocation][xLocation] = currentNum;
			currentNum += 1;
			yLocation -= 1;
		}

		//Move right until you reach a blank location below
		while(!(grid[yLocation + 1][xLocation] == 0)){
			grid[yLocation][xLocation] = currentNum;
			currentNum += 1;
			xLocation += 1;
			//Check if you are at the final location and break the loop if you are
			if(xLocation == grid.len()){
				finalLocation = true;
				break;
			}
		}
	}

	return grid;
}
//Finds the sum of the diagonals in the grid
fn findSum(grid: Vec::<Vec::<i64>>) -> i64{
	//Start at the top corners and work your way down moving toward the opposite side
	let mut sumOfDiagonals = 0;
	let mut leftSide: i32 = 0;
	let mut rightSide: i32 = (grid.len() - 1) as i32;
	let mut row: i32 = 0;
	//println!("grid {}", grid.len());

	while(row < grid.len() as i32){
		//This ensures the middle location is only counted once
		if(leftSide == rightSide){
			sumOfDiagonals += grid[row as usize][leftSide as usize];
		}
		else{
			sumOfDiagonals += grid[row as usize][leftSide as usize];
			sumOfDiagonals += grid[row as usize][rightSide as usize];
		}
		row += 1;
		leftSide += 1;
		rightSide -= 1;
	}

	//Return the sum
	return sumOfDiagonals;
}

/* Results:
The sum of the diagonals in the given grid is 669171001
It took an average of 7.760 milliseconds to run this problem through 100 iterations
*/
