//ProjectEulerRust/src/Problems/Problems1.rs
//Matthew Ellison
// Created: 06-12-20
//Modified: 10-26-20
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> &'static str{
	"What is the sum of all the multiples of 3 or 5 that are less than 1000"
}

pub fn solve() -> Answer{
	let mut timer = myClasses::Stopwatch::Stopwatch::new();

	//Start the timer
	timer.start();

	//Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
	let fullSum = sumOfProgression(3.0) + sumOfProgression(5.0) - sumOfProgression(3.0 * 5.0);

	//Top the timer
	timer.stop();

	//Return the answer
	return Answer::new("The sum of all numbers < 1000 is ".to_string() + &fullSum.to_string(), timer.getString(), timer.getNano());
}

fn sumOfProgression(multiple: f64) -> i64{
	let numTerms = (999f64 / multiple).floor();	//Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
	//The sum of progression formula is (n / 2)(a + l). n = number of terms, a = multiple, l = last term
	return ((numTerms / 2f64) * (multiple + (numTerms * multiple))) as i64;
}

/* Results:
The sum of all numbers < 1000 is 233168
It took an average of 868.000 nanoseconds to run this problem through 100 iterations
*/
