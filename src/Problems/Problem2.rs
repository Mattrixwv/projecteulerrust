//ProjectEulerRust/src/Problems/Problems2.rs
//Matthew Ellison
// Created: 06-12-20
//Modified: 07-21-20
//The sum of the even Fibonacci numbers less than 4,000,000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> &'static str{
	"What is the sum of the even Fibonacci numbers less than 4,000,000?"
}

pub fn solve() -> Answer{
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();
	//Get a list of all fibonacci numbers < 4,000,000
	let fibNums = myClasses::Algorithms::getAllFib(3_999_999);
	let mut sum = 0;
	//Set through every element in the list checking if it is even
	for num in fibNums{
		//If the number is even add it to the running tally
		if((num % 2) == 0){
			sum += num;
		}
	}
	timer.stop();

	//Return the answer
	return Answer::new("The sum of all even fibonacci numbers <= 3999999 is ".to_string() + &sum.to_string(), timer.getString(), timer.getNano());
}

/* Results:
The sum of all even fibonacci numbers <= 3999999 is 4613732
It took an average of 1.584 microseconds to run this problem through 100 iterations
*/
