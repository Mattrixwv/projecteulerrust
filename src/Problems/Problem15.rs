//ProjectEulerRust/src/Problems/Problems15.rs
//Matthew Ellison
// Created: 06-16-20
//Modified: 07-21-20
//How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


static WIDTH: i32 = 20;
static LENGTH: i32 = 20;

pub fn getDescription() -> String{
	"How many routes from the top left corner to the bottom right corner are there through a 20x20 grid if you can only move right and down?".to_string()
}

pub fn solve() -> Answer{
	//Setup the rest of the variables
	let currentX = 0;	//The current x location on the grid
	let currentY = 0;	//The current y location on the grid
	let mut numOfRoutes = 0i64;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//We write this as a recursive function
	//When in a location it always move right first, then down
	moveLoc(currentX, currentY, &mut numOfRoutes);

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The number of routes is {}", numOfRoutes), timer.getString(), timer.getNano());
}

//This function acts as a handler for moving the position on the grid and counting the distance
//It moves right first, then down
fn moveLoc(currentX: i32, currentY: i32, numOfRoutes: &mut i64){
	//Check if you are at the end and act accordingly
	if((currentX == WIDTH) && (currentY == LENGTH)){
		*numOfRoutes += 1;
		return;
	}

	//Move right if possible
	if(currentX < WIDTH){
		moveLoc(currentX + 1, currentY, numOfRoutes);
	}
	//Move down if possible{
	if(currentY < LENGTH){
		moveLoc(currentX, currentY + 1, numOfRoutes);
	}
}

/* Results:
The number of routes is 137846528820
It took an average of 12.199 minutes to run this problem through 10 iterations
*/
