//ProjectEulerRust/src/Problems/Problems8.rs
//Matthew Ellison
// Created: 06-15-20
//Modified: 07-21-20
//Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?
/*
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"What is the 10001th prime number?".to_string()
}

pub fn solve() -> Answer{
	//Setup the variables
	let NUMBER = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";
	let mut maxNums = "".to_string();
	let mut maxProduct = 0u64;	//u64 because 2^32 < 9^13

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Cycle through the string of number looking for the maximum product
	for cnt in 12..NUMBER.len(){
		let currentProduct = NUMBER.chars().nth(cnt - 12).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 11).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 10).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 9).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 8).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 7).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 6).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 5).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 4).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 3).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 2).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt - 1).unwrap().to_digit(10).unwrap() as u64 * NUMBER.chars().nth(cnt).unwrap().to_digit(10).unwrap() as u64;
		//Check if the product is greater than the current maximum
		if(currentProduct > maxProduct){
			maxNums = NUMBER.chars().skip(cnt - 12).take(13).collect();
			maxProduct = currentProduct;
		}
	}

	//Start the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The greatest product is {}\nThe numbers are {}", maxProduct, maxNums), timer.getString(), timer.getNano());
}

/* Results:
The greatest product is 23514624000
The numbers are 5576689664895
It took an average of 3.162 milliseconds to run this problem through 100 iterations
*/
