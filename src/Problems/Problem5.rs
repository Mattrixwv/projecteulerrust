//ProjectEulerRust/src/Problems/Problems5.rs
//Matthew Ellison
// Created: 06-15-20
//Modified: 07-21-20
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?".to_string()
}

pub fn solve() -> Answer{
	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Start at 20 because it must at least be divisible by 20. Increment by 2 because it must be an even number to be divisible by 2
	let mut numFound = false;
	let mut currentNum = 20;
	//If the number is < 0 it has overflowed
	while((currentNum > 0) && (!numFound)){
		//Start by assuming you found the number (because we throw a flad if we didn't find it)
		numFound = true;
		//Step through every number from 1-20 seeing if the current number is divisible by it
		for divisor in 1..=20{
			if((currentNum % divisor) != 0){
				numFound = false;
				break;
			}
		}
		//If you didn't find the correct number then increment by 2
		if(!numFound){
			currentNum += 2;
		}
	}

	//Stop the timer
	timer.stop();

	//Save the results
	return Answer::new(format!("The smallest positive number evenly divisibly by all number 1-20 is {}", currentNum), timer.getString(), timer.getNano());
}

/* Results:
The smallest positive number evenly divisibly by all number 1-20 is 232792560
It took an average of 2.535 seconds to run this problem through 100 iterations
*/
