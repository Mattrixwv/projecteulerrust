//ProjectEulerRust/src/Problems/Problems34.rs
//Matthew Ellison
// Created: 06-01-21
//Modified: 06-01-21
//Find the sum of all numbers which are equal to the sum of the factorial of their digits
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"Find the sum of all numbers which are equal to the sum of the factorial of their digits".to_string()
}

pub fn solve() -> Answer{
	let maxNum = 1499999;
	let mut factorials = Vec::<i64>::new();
	let mut sum = 0;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Pre-compute the possible factorials from 0! to 9!
	for cnt in 0 ..= 9{
		factorials.push(myClasses::Algorithms::factorial(cnt));
	}
	//Run through all possible number form 3-MAX_NUM and see if they equal the sum of their digit's factorials
	for cnt in 3 ..= maxNum{
		//Split the number into its digits and add each one to the sum
		let numString = cnt.to_string();
		let mut currentSum = 0;
		for numChar in numString.chars(){
			currentSum += factorials[numChar.to_digit(10).unwrap() as usize];
		}
		//If the number is equal to the sum add the sum to the running sum
		if(currentSum == cnt){
			sum += currentSum;
		}
	}

	//Stop the timer
	timer.stop();

	return Answer::new(format!("The sum of all numbers that are the sum of their digit's factorials is {}", sum), timer.getString(), timer.getNano());
}

/* Results
The sum of all numbers that are the sum of their digit's factorials is 40730
It took an average of 143.433 milliseconds to run this problem through 100 iterations
*/
