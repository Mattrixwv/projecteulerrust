//ProjectEuler/ProjectEulerRust/src/Problems/Problems32.rs
//Matthew Ellison
// Created: 07-28-20
//Modified: 07-28-20
//Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.".to_string()
}

#[derive(Copy, Clone)]
pub struct ProductSet{
	pub multiplicand: i32,
	pub multiplier: i32
}
impl ProductSet{
	pub fn new(first: i32, second: i32) -> ProductSet{
		let set = ProductSet{multiplicand: first, multiplier: second};
		return set;
	}
	pub fn getProduct(&self) -> i64{
		return (self.multiplicand as i64 * self.multiplier as i64);
	}
	pub fn getNumString(&self) -> String{
		return format!("{}{}{}", self.multiplicand, self.multiplier, self.getProduct());
	}
}
impl PartialEq for ProductSet{
	fn eq(&self, other: &Self) -> bool{
		return (self.getProduct() == other.getProduct());
	}
}

//Solve the problem
pub fn solve() -> Answer{
	//Setup the variables
	let TOP_MULTIPLICAND = 99;	//The largest multiplicand to check
	let TOP_MULTIPLIER = 4999;	//The largest multiplier to check
	let mut listOfProducts = Vec::<ProductSet>::new();	//The list of unique products that are 1-9 pandigital

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Create the multiplicand and start working your way up
	for multiplicand in 1..=TOP_MULTIPLICAND{
		//Run through all possible multipliers
		for multiplier in multiplicand..=TOP_MULTIPLIER{
			let currentProductSet = ProductSet::new(multiplicand, multiplier);
			//If the product is too long move on to the next possible number
			if(currentProductSet.getNumString().len() > 9){
				break;
			}
			//If the current number is a pandigital that doesn't already exist in the list add it
			if(isPandigital(&currentProductSet)){
				if(!listOfProducts.contains(&currentProductSet)){
					listOfProducts.push(currentProductSet);
				}
			}
		}
	}

	//Get the sum of the products of the pandigitals
	let mut sumOfPandigitals = 0;
	let numOfPandigitals = listOfProducts.len();	//This exists because it doesn't like to put this value in the format statement after the for loop
	for prod in listOfProducts{
		sumOfPandigitals += prod.getProduct();
	}

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("There are {} unique 1-9 pandigitals\nThe sum of the products of the pandigitals is {}", numOfPandigitals, sumOfPandigitals), timer.getString(), timer.getNano());
}
//Returns true if the passed productset is 1-9 pandigital
pub fn isPandigital(currentSet: &ProductSet) -> bool{
	//Get the numbers out of the object and put them into a string
	let numberString = currentSet.getNumString();
	//Make sure the string is the correct length
	if(numberString.len() != 9){
		return false;
	}
	//Make sure every number from 1-9 is contained exactly once
	for panNumber in 1..=9{
		//Make sure there is exactly one of this number contained in the string
		if(numberString.matches(format!("{}", panNumber).as_str()).count() != 1){
			return false;
		}
	}
	//If all numbers were found in the string return true
	return true;
}

/* Results:
There are 7 unique 1-9 pandigitals
The sum of the products of the pandigitals is 45228
It took an average of 28.795 milliseconds to run this problem through 100 iterations
*/
