//ProjectEulerRust/src/Problems/Problems12.rs
//Matthew Ellison
// Created: 06-16-20
//Modified: 07-21-20
//What is the value of the first triangle number to have over five hundred divisors?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"What is the value of the first triangle number to have over five hundred divisors?".to_string()
}

pub fn solve() -> Answer{
	//Setup the other variables
	let GOAL_DIVISORS = 500i64;
	let mut foundNumber = false;	//To flag whether the number has been found
	let mut sum = 1i64;
	let mut counter = 2i64;	//The next number to be added to the sum to make a triangular number
	let mut divisors = Vec::<i64>::new();

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Loop until you find the appropriate number
	while((!foundNumber) && (sum > 0)){
		divisors = myClasses::Algorithms::getDivisors(sum);
		//If the number of divisors is correct set the flag
		if(divisors.len() as i64 > GOAL_DIVISORS){
			foundNumber = true;
		}
		//Otherwise add to the sum and increase the next number
		else{
			sum += counter;
			counter += 1;
		}
	}

	//Stop the timer
	timer.stop();

	//Return the solution
	return Answer::new(format!("The triangular number {} is the sum of all numbers >= {} and has {} divisors", sum, counter - 1, divisors.len()), timer.getString(), timer.getNano());
}

/* Results:
The triangular number 76576500 is the sum of all numbers >= 12375 and has 576 divisors
It took an average of 272.345 milliseconds to run this problem through 100 iterations
*/
