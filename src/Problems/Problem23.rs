//ProjectEulerRust/src/Problems/Problems23.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers".to_string()
}

pub fn solve() -> Answer{
	let MAX_NUM = 28123;
	//Setup the variables
	let mut divisorSums = Vec::<i64>::new();	//Holds the sum of all the divisors of a number
	//Make sure every element has a 0 in it's location
	for _ in 0..=MAX_NUM{
		divisorSums.push(0);
	}

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Get the sum of the divisors of all numbers < MAX_NUM
	for cnt in 1..MAX_NUM{
		let mut div = myClasses::Algorithms::getDivisors(cnt);
		//Remove the last element, which is the number itself. This gives us the propper divisors
		if(div.len() > 1){
			div.remove(div.len() - 1);
		}
		divisorSums[cnt as usize] = div.iter().sum();
	}

	//Get the abundant numbers
	let mut abund = Vec::<i64>::new();
	for cnt in 0..divisorSums.len(){
		if(divisorSums[cnt] > cnt as i64){
			abund.push(cnt as i64);
		}
	}

	//Check if each number can be the sum of 2 abundant numbers and add to the sum if no
	let mut sum = 0;
	for cnt in 1..MAX_NUM{
		if(!isSum(&abund, cnt)){
			sum += cnt;
		}
	}

	//Stop the timer
	timer.stop();

	//Return the result
	return Answer::new(format!("The answer is {}", sum), timer.getString(), timer.getNano());
}

fn isSum(abund: &Vec<i64>, num: i64) -> bool{
	//Pick a number for the first part of the sum
	for firstNum in 0..abund.len(){
		//Pick a number for the second part of the sum
		for secondNum in firstNum..abund.len(){
			let sum = abund[firstNum] + abund[secondNum];
			if(sum == num){
				return true;
			}
			else if(sum > num){
				break;
			}
		}
	}
	//If you have run through the entire list and did not find a sum then it is false
	return false;
}

/* Results:
The answer is 4179871
It took an average of 4.845 seconds to run this problem through 100 iterations
*/
