//ProjectEulerRust/src/Problems/Problems30.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.".to_string()
}

pub fn solve() -> Answer{
	let TOP_NUM = 1_000_000;	//This is the largest number that will be checked
	let BOTTOM_NUM = 2;	//Starts with 2 because 0 and 1 don't count
	let POWER_RAISED = 5;	//This is the power that the digits are raised to
	let mut sumOfFifthNumbers = Vec::<i64>::new();	//Sum of the power of the fifth powers of their digits

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Start with the lowest number and increment until you reach the largest number
	for currentNum in BOTTOM_NUM..=TOP_NUM{
		//Get the digits of the number
		let digits = getDigits(currentNum);
		//Get the sum of the powers
		let mut sumOfPowers = 0;
		for num in digits{
			sumOfPowers += num.pow(POWER_RAISED);
		}
		//Check if the sum of the powers is the same as the number
		//If it is add it to the list, otherwise continue to the next number
		if(sumOfPowers == currentNum){
			sumOfFifthNumbers.push(currentNum);
		}
	}

	let sum = sumOfFifthNumbers.iter().sum::<i64>();

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The sum of all the numbers that can be written as the sum of the fifth powers of their digits is {}", sum), timer.getString(), timer.getNano());
}
//Returns a vector with the individual digits of the number passed to it
fn getDigits(num: i64) -> Vec::<i64>{
	let mut listOfDigits = Vec::<i64>::new();	//This ArrayList holds the individual digits of num
	//The easiest way to get the individual digits of a number is by converting it to a string
	let digits = num.to_string();
	//Start with the first digit, convert it to an integer, store it in the ArrayList, and move to the next digit
	for character in digits.chars(){
		listOfDigits.push(character.to_digit(10).unwrap() as i64);
	}
	//Return the list of digits
	return listOfDigits;
}

/* Results:
The sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839
It took an average of 319.230 milliseconds to run this problem through 100 iterations
*/
