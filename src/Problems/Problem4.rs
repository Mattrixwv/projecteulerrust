//ProjectEulerRust/src/Problems/Problems4.rs
//Matthew Ellison
// Created: 06-14-20
//Modified: 07-21-20
//Find the largest palindrome made from the product of two 3-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"Find the largest palindrome made from the product of two 3-digit numbers".to_string()
}

pub fn solve() -> Answer{
	let START_NUM = 100;
	let END_NUM = 999;
	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	let mut palindromes = Vec::<i64>::new();

	//Start at the first 3-digit number and check every one up to the last 3-digit number
	for firstNum in START_NUM..END_NUM{
		//You can start at the location of the first number because everything before that has already been tested. (100 * 101 == 101 * 100)
		for secondNum in firstNum..END_NUM{
			//Get the product of the numbers
			let product = firstNum * secondNum;
			//Change the number into a string
			let productString: String = product.to_string();
			//Reverse the string
			let reverseString: String = productString.chars().rev().collect();

			//If the number and it's reverse are the same it is a palindrome so add it to the list
			if(productString == reverseString){
				palindromes.push(product);
			}
			//If it's not a palindrome ignore it and move to the next number
		}
	}

	//Stop the timer
	timer.stop();

	//Sort the palindromes so that the last one is the largest
	palindromes.sort();

	//Return the results
	return Answer::new(format!("The largest palindrome is {}", palindromes[palindromes.len() - 1]), timer.getString(), timer.getNano());
}

/* Results:
The largest palindrome is 906609
It took an average of 114.371 milliseconds to run this problem through 100 iterations
*/
