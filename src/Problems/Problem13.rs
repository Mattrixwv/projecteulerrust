//ProjectEulerRust/src/Problems/Problems13.rs
//Matthew Ellison
// Created: 06-16-20
//Modified: 07-21-20
//Work out the first ten digits of the sum of the following one-hundred 50-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"Work out the first ten digits of the sum of the one-hundred 50-digit numbers".to_string()
}

pub fn solve() -> Answer{
	let mut nums = Vec::<num::BigInt>::new();

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Setup the array
	nums.push("37107287533902102798797998220837590246510135740250".parse::<num::BigInt>().unwrap());
	nums.push("46376937677490009712648124896970078050417018260538".parse::<num::BigInt>().unwrap());
	nums.push("74324986199524741059474233309513058123726617309629".parse::<num::BigInt>().unwrap());
	nums.push("91942213363574161572522430563301811072406154908250".parse::<num::BigInt>().unwrap());
	nums.push("23067588207539346171171980310421047513778063246676".parse::<num::BigInt>().unwrap());
	nums.push("89261670696623633820136378418383684178734361726757".parse::<num::BigInt>().unwrap());
	nums.push("28112879812849979408065481931592621691275889832738".parse::<num::BigInt>().unwrap());
	nums.push("44274228917432520321923589422876796487670272189318".parse::<num::BigInt>().unwrap());
	nums.push("47451445736001306439091167216856844588711603153276".parse::<num::BigInt>().unwrap());
	nums.push("70386486105843025439939619828917593665686757934951".parse::<num::BigInt>().unwrap());
	nums.push("62176457141856560629502157223196586755079324193331".parse::<num::BigInt>().unwrap());
	nums.push("64906352462741904929101432445813822663347944758178".parse::<num::BigInt>().unwrap());
	nums.push("92575867718337217661963751590579239728245598838407".parse::<num::BigInt>().unwrap());
	nums.push("58203565325359399008402633568948830189458628227828".parse::<num::BigInt>().unwrap());
	nums.push("80181199384826282014278194139940567587151170094390".parse::<num::BigInt>().unwrap());
	nums.push("35398664372827112653829987240784473053190104293586".parse::<num::BigInt>().unwrap());
	nums.push("86515506006295864861532075273371959191420517255829".parse::<num::BigInt>().unwrap());
	nums.push("71693888707715466499115593487603532921714970056938".parse::<num::BigInt>().unwrap());
	nums.push("54370070576826684624621495650076471787294438377604".parse::<num::BigInt>().unwrap());
	nums.push("53282654108756828443191190634694037855217779295145".parse::<num::BigInt>().unwrap());
	nums.push("36123272525000296071075082563815656710885258350721".parse::<num::BigInt>().unwrap());
	nums.push("45876576172410976447339110607218265236877223636045".parse::<num::BigInt>().unwrap());
	nums.push("17423706905851860660448207621209813287860733969412".parse::<num::BigInt>().unwrap());
	nums.push("81142660418086830619328460811191061556940512689692".parse::<num::BigInt>().unwrap());
	nums.push("51934325451728388641918047049293215058642563049483".parse::<num::BigInt>().unwrap());
	nums.push("62467221648435076201727918039944693004732956340691".parse::<num::BigInt>().unwrap());
	nums.push("15732444386908125794514089057706229429197107928209".parse::<num::BigInt>().unwrap());
	nums.push("55037687525678773091862540744969844508330393682126".parse::<num::BigInt>().unwrap());
	nums.push("18336384825330154686196124348767681297534375946515".parse::<num::BigInt>().unwrap());
	nums.push("80386287592878490201521685554828717201219257766954".parse::<num::BigInt>().unwrap());
	nums.push("78182833757993103614740356856449095527097864797581".parse::<num::BigInt>().unwrap());
	nums.push("16726320100436897842553539920931837441497806860984".parse::<num::BigInt>().unwrap());
	nums.push("48403098129077791799088218795327364475675590848030".parse::<num::BigInt>().unwrap());
	nums.push("87086987551392711854517078544161852424320693150332".parse::<num::BigInt>().unwrap());
	nums.push("59959406895756536782107074926966537676326235447210".parse::<num::BigInt>().unwrap());
	nums.push("69793950679652694742597709739166693763042633987085".parse::<num::BigInt>().unwrap());
	nums.push("41052684708299085211399427365734116182760315001271".parse::<num::BigInt>().unwrap());
	nums.push("65378607361501080857009149939512557028198746004375".parse::<num::BigInt>().unwrap());
	nums.push("35829035317434717326932123578154982629742552737307".parse::<num::BigInt>().unwrap());
	nums.push("94953759765105305946966067683156574377167401875275".parse::<num::BigInt>().unwrap());
	nums.push("88902802571733229619176668713819931811048770190271".parse::<num::BigInt>().unwrap());
	nums.push("25267680276078003013678680992525463401061632866526".parse::<num::BigInt>().unwrap());
	nums.push("36270218540497705585629946580636237993140746255962".parse::<num::BigInt>().unwrap());
	nums.push("24074486908231174977792365466257246923322810917141".parse::<num::BigInt>().unwrap());
	nums.push("91430288197103288597806669760892938638285025333403".parse::<num::BigInt>().unwrap());
	nums.push("34413065578016127815921815005561868836468420090470".parse::<num::BigInt>().unwrap());
	nums.push("23053081172816430487623791969842487255036638784583".parse::<num::BigInt>().unwrap());
	nums.push("11487696932154902810424020138335124462181441773470".parse::<num::BigInt>().unwrap());
	nums.push("63783299490636259666498587618221225225512486764533".parse::<num::BigInt>().unwrap());
	nums.push("67720186971698544312419572409913959008952310058822".parse::<num::BigInt>().unwrap());
	nums.push("95548255300263520781532296796249481641953868218774".parse::<num::BigInt>().unwrap());
	nums.push("76085327132285723110424803456124867697064507995236".parse::<num::BigInt>().unwrap());
	nums.push("37774242535411291684276865538926205024910326572967".parse::<num::BigInt>().unwrap());
	nums.push("23701913275725675285653248258265463092207058596522".parse::<num::BigInt>().unwrap());
	nums.push("29798860272258331913126375147341994889534765745501".parse::<num::BigInt>().unwrap());
	nums.push("18495701454879288984856827726077713721403798879715".parse::<num::BigInt>().unwrap());
	nums.push("38298203783031473527721580348144513491373226651381".parse::<num::BigInt>().unwrap());
	nums.push("34829543829199918180278916522431027392251122869539".parse::<num::BigInt>().unwrap());
	nums.push("40957953066405232632538044100059654939159879593635".parse::<num::BigInt>().unwrap());
	nums.push("29746152185502371307642255121183693803580388584903".parse::<num::BigInt>().unwrap());
	nums.push("41698116222072977186158236678424689157993532961922".parse::<num::BigInt>().unwrap());
	nums.push("62467957194401269043877107275048102390895523597457".parse::<num::BigInt>().unwrap());
	nums.push("23189706772547915061505504953922979530901129967519".parse::<num::BigInt>().unwrap());
	nums.push("86188088225875314529584099251203829009407770775672".parse::<num::BigInt>().unwrap());
	nums.push("11306739708304724483816533873502340845647058077308".parse::<num::BigInt>().unwrap());
	nums.push("82959174767140363198008187129011875491310547126581".parse::<num::BigInt>().unwrap());
	nums.push("97623331044818386269515456334926366572897563400500".parse::<num::BigInt>().unwrap());
	nums.push("42846280183517070527831839425882145521227251250327".parse::<num::BigInt>().unwrap());
	nums.push("55121603546981200581762165212827652751691296897789".parse::<num::BigInt>().unwrap());
	nums.push("32238195734329339946437501907836945765883352399886".parse::<num::BigInt>().unwrap());
	nums.push("75506164965184775180738168837861091527357929701337".parse::<num::BigInt>().unwrap());
	nums.push("62177842752192623401942399639168044983993173312731".parse::<num::BigInt>().unwrap());
	nums.push("32924185707147349566916674687634660915035914677504".parse::<num::BigInt>().unwrap());
	nums.push("99518671430235219628894890102423325116913619626622".parse::<num::BigInt>().unwrap());
	nums.push("73267460800591547471830798392868535206946944540724".parse::<num::BigInt>().unwrap());
	nums.push("76841822524674417161514036427982273348055556214818".parse::<num::BigInt>().unwrap());
	nums.push("97142617910342598647204516893989422179826088076852".parse::<num::BigInt>().unwrap());
	nums.push("87783646182799346313767754307809363333018982642090".parse::<num::BigInt>().unwrap());
	nums.push("10848802521674670883215120185883543223812876952786".parse::<num::BigInt>().unwrap());
	nums.push("71329612474782464538636993009049310363619763878039".parse::<num::BigInt>().unwrap());
	nums.push("62184073572399794223406235393808339651327408011116".parse::<num::BigInt>().unwrap());
	nums.push("66627891981488087797941876876144230030984490851411".parse::<num::BigInt>().unwrap());
	nums.push("60661826293682836764744779239180335110989069790714".parse::<num::BigInt>().unwrap());
	nums.push("85786944089552990653640447425576083659976645795096".parse::<num::BigInt>().unwrap());
	nums.push("66024396409905389607120198219976047599490197230297".parse::<num::BigInt>().unwrap());
	nums.push("64913982680032973156037120041377903785566085089252".parse::<num::BigInt>().unwrap());
	nums.push("16730939319872750275468906903707539413042652315011".parse::<num::BigInt>().unwrap());
	nums.push("94809377245048795150954100921645863754710598436791".parse::<num::BigInt>().unwrap());
	nums.push("78639167021187492431995700641917969777599028300699".parse::<num::BigInt>().unwrap());
	nums.push("15368713711936614952811305876380278410754449733078".parse::<num::BigInt>().unwrap());
	nums.push("40789923115535562561142322423255033685442488917353".parse::<num::BigInt>().unwrap());
	nums.push("44889911501440648020369068063960672322193204149535".parse::<num::BigInt>().unwrap());
	nums.push("41503128880339536053299340368006977710650566631954".parse::<num::BigInt>().unwrap());
	nums.push("81234880673210146739058568557934581403627822703280".parse::<num::BigInt>().unwrap());
	nums.push("82616570773948327592232845941706525094512325230608".parse::<num::BigInt>().unwrap());
	nums.push("22918802058777319719839450180888072429661980811197".parse::<num::BigInt>().unwrap());
	nums.push("77158542502016545090413245809786882778948721859617".parse::<num::BigInt>().unwrap());
	nums.push("72107838435069186155435662884062257473692284509516".parse::<num::BigInt>().unwrap());
	nums.push("20849603980134001723930671666823555245252804609722".parse::<num::BigInt>().unwrap());
	nums.push("53503534226472524250874054075591789781264330331690".parse::<num::BigInt>().unwrap());

	//Get the sum of all the numbers
	let sum =  nums.iter().sum::<num::BigInt>();

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The sum of all {} numbers is {}\nThe first 10 digits of the sum of the numbers is {}", nums.len(), sum, sum.to_str_radix(10).chars().take(10).collect::<String>()), timer.getString(), timer.getNano());
}

/* Results:
The sum of all 100 numbers is 5537376230390876637302048746832985971773659831892672
The first 10 digits of the sum of the numbers is 5537376230
It took an average of 28.830 microseconds to run this problem through 100 iterations
*/
