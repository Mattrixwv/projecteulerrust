//ProjectEulerRust/src/Problems/Problems24.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?".to_string()
}

pub fn solve() -> Answer{
	let NEEDED_PERM = 1_000_000;
	//Setup the variables
	let nums = "0123456789".to_string();	//The string that you are trying to find the permutations of

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Get all the permutations of the string
	let permutations = myClasses::Algorithms::getPermutations(nums);

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The 1 millionth permutation is {}", permutations[NEEDED_PERM - 1]), timer.getString(), timer.getNano());
}

/* Results:
The 1 millionth permutation is 2783915460
It took an average of 14.048 seconds to run this problem through 100 iterations
*/
