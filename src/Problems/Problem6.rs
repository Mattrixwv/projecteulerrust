//ProjectEulerRust/src/Problems/Problems6.rs
//Matthew Ellison
// Created: 06-15-20
//Modified: 07-21-20
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;


pub fn getDescription() -> String{
	"Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.".to_string()
}

pub fn solve() -> Answer{
	let START_NUM = 1;
	let END_NUM = 100;
	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Setup the variables
	let mut sumOfSquares = 0i64;	//Holds the sum of the squares of all the numbers
	let mut squareOfSum = 0i64;		//Holds the square of the sum of all the numbers

	//Run through all number and add them to the appropriate sums
	for currentNum in START_NUM..END_NUM{
		//Add the square to the correct variable
		sumOfSquares += (currentNum * currentNum);
		//Add the number to the correct variable to squaring later
		squareOfSum += currentNum;
	}
	//Squaring the sum that needs it
	squareOfSum *= squareOfSum;

	//Stop the timer
	timer.stop();

	//Save the results
	return Answer::new(format!("The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is {}", (sumOfSquares - squareOfSum).abs()), timer.getString(), timer.getNano());
}

/* Results:
The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is 24174150
It took an average of 50.000 nanoseconds to run this problem through 100 iterations
*/
