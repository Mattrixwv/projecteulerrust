//ProjectEulerRust/src/Problems/Problems20.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//What is the sum of the digits of 100!?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"What is the sum of the digits of 100!?".to_string()
}

pub fn solve() -> Answer{
	let TOP_NUM = 100;
	//The number that is being generated
	let mut number = num::BigInt::from(1);
	let mut sum = 0;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Run through every number from 1 to 100 and multiply it by the current num to generate 100!
	for cnt in (2..=TOP_NUM).rev(){
		number *= num::BigInt::from(cnt);
	}

	//Get a string of the number because it is easier to pull apart the individual characters
	let numString = number.to_string();
	//Run through every character in the string, convert it back to an integer and add it to the running sum
	for cnt in numString.chars(){
		sum += cnt.to_digit(10).unwrap();
	}

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("{}! = {}\nThe sum of the digits is: {}", TOP_NUM, number, sum), timer.getString(), timer.getNano());
}

/* Results:
100! = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
The sum of the digits is: 648
It took an average of 15.565 microseconds to run this problem through 100 iterations
*/
