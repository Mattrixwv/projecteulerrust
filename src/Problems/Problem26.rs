//ProjectEulerRust/src/Problems/Problems26.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.".to_string()
}

pub fn solve() -> Answer{
	let TOP_NUM = 999;
	//The length of the longest cycle
	let mut longestCycle = 0;
	//The starting denominator of the longest cycle
	let mut longestNumber = 0;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Start with 1/2 and find out how long the longest cycle is by checking the remainders
	//Loop through every number from 2-999 and use it for the denominator
	for denominator in 2..=TOP_NUM{
		let mut denomList = Vec::<i64>::new();
		let mut endFound = false;	//Whether we have found an end to the number (either a cycle or a 0 for remainder)
		let mut cycleFound = false;	//Whether a cycle was detected
		let mut numerator = 1;	//The numerator that will be divided. Always starts at 1
		while(!endFound){
			//Get the remainder after the division
			let remainder = numerator % denominator;
			//Check if the remainder is 0
			//If it is set the flag
			if(remainder == 0){
				endFound = true;
			}
			//Check if the remainder is in the list
			//If it is in the list, set the appropriate flags
			else if(denomList.contains(&remainder)){
				endFound = true;
				cycleFound = true;
			}
			//Else add it to the list
			else{
				denomList.push(remainder);
			}
			//Multiply the remainder by 10 to continue finding the next remainder
			numerator = remainder * 10;
		}
		//If a cycle was found check the size of the list against the largest cycle
		if(cycleFound){
			//If it is larger than the largest, set it as the new largest
			if(denomList.len() > longestCycle){
				longestCycle = denomList.len();
				longestNumber = denominator;
			}
		}
	}

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The longest cycle is {} digits long\nIt started with the number {}", longestCycle, longestNumber), timer.getString(), timer.getNano());
}

/* Results:
The longest cycle is 982 digits long
It started with the number 983
It took an average of 7.834 milliseconds to run this problem through 100 iterations
*/
