//ProjectEulerRust/src/Problems/Problems31.rs
//Matthew Ellison
// Created: 06-19-20
//Modified: 07-21-20
//How many different ways can £2 be made using any number of coins?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.".to_string()
}

pub fn solve() -> Answer{
	let desiredValue = 200;
	let mut permutations = 0;

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Start with 200p and remove the necessary coins with each loop
	for pound2 in (0..=desiredValue).rev().step_by(200){
		for pound1 in (0..=pound2).rev().step_by(100){
			for pence50 in (0..=pound1).rev().step_by(50){
				for pence20 in (0..=pence50).rev().step_by(20){
					for pence10 in (0..=pence20).rev().step_by(10){
						for pence5 in (0..=pence10).rev().step_by(5){
							for _pence2 in (0..=pence5).rev().step_by(2){
								permutations += 1;
							}
						}
					}
				}
			}
		}
	}

	//Stop the timer
	timer.stop();

	return Answer::new(format!("There are {} ways to make 2 pounds with the given denominations of coins", permutations), timer.getString(), timer.getNano());
}

/* Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took an average of 100.789 microseconds to run this problem through 100 iterations
*/
