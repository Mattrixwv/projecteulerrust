//ProjectEulerRust/src/Problems/Problems27.rs
//Matthew Ellison
// Created: 06-17-20
//Modified: 07-21-20
//Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


extern crate myClasses;
use crate::Problems::Answer::Answer;

pub fn getDescription() -> String{
	"Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0".to_string()
}

pub fn solve() -> Answer{
	let mut topA = 0;	//The A for the most n's generated
	let mut topB = 0;	//The B for the most n's generated
	let mut topN = 0;	//The most n's generated
	let primes = myClasses::Algorithms::getPrimes(12000);	//A list of all primes that could possible be generated with this formula

	//Start the timer
	let mut timer = myClasses::Stopwatch::Stopwatch::new();
	timer.start();

	//Start with the lowest possible A and check all possibilities after that
	for a in -999..=999{
		//Start with the lowest possible B and check all possibilities after that
		for b in -1000..=1000{
			//Start with n=0 and check the formula to see how many primes you can get with concecutive n's
			let mut n = 0;
			let mut quadratic = (n * n) + (a * n) + b;
			while(primes.contains(&quadratic)){
				n += 1;
				quadratic = (n * n) + (a * n) + b;
			}
			n -= 1;	//Remove an n because the last formula failed

			//Set all the largest number if this created more primes than any other
			if(n > topN){
				topN = n;
				topB = b;
				topA = a;
			}
		}
	}

	//Stop the timer
	timer.stop();

	//Return the results
	return Answer::new(format!("The greatest number of primes found is {}\nIt was found with A = {}, B = {}\nTHe product of A and B is {}", topN, topA, topB, topA * topB), timer.getString(), timer.getNano());
}

/* Results:
The greatest number of primes found is 70
It was found with A = -61, B = 971
THe product of A and B is -59231
It took an average of 1.840 seconds to run this problem through 100 iterations
*/
