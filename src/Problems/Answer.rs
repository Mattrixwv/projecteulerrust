//ProjectEulerRust/src/Problems/Answer.rs
//Matthew Ellison
// Created: 06-11-20
//Modified: 06-15-20
//A structure to hold the answer to the problem
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#[allow(dead_code)]


pub struct Answer{
	result: String,
	time: String,
	nanoseconds: u128,
}
impl std::fmt::Display for Answer{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result{
		write!(f, "{}\nIt took {} to solve this problem", self.result, self.time)
	}
}
impl Answer{
	pub fn new(result: String, time: String, nanoseconds: u128) -> Answer{
		Answer{result, time, nanoseconds}
	}
	pub fn getResult(&self) -> String{
		return format!("{}", self.result).to_string();
	}
	pub fn getTime(&self) -> String{
		return format!("{}", self.time).to_string();
	}
	pub fn getNano(&self) -> u128{
		return self.nanoseconds;
	}
}
