//ProjectEulerRust/src/main.rs
//Matthew Ellison
// Created: 06-11-20
//Modified: 06-18-20
//This is a driver function for the Project Euler solutions in Rust
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#![allow(non_snake_case)]
#![allow(unused_parens)]
#![allow(non_upper_case_globals)]


mod Problems;
extern crate myClasses;


use std::io::Write;


#[derive(PartialEq)]
enum Selections{EMPTY, SOLVE, DESCRIPTION, LIST, BENCHMARK, EXIT, SIZE}
#[allow(non_camel_case_types)]
#[derive(PartialEq)]
enum BenchmarkOptions{empty, runSpecific, runAllShort, runAll, exit, size}


fn main(){
	let mut selection = Selections::EMPTY;
	while(selection != Selections::EXIT){
		printMenu();
		selection = getMenuSelection();

		if(selection == Selections::SOLVE){
			solveMenu();
		}
		else if(selection == Selections::DESCRIPTION){
			descriptionMenu();
		}
		else if(selection == Selections::LIST){
			Problems::listProblems();
		}
		else if(selection == Selections::BENCHMARK){
			benchmarkMenu();
		}
		else if(selection == Selections::EXIT){
		}
		else{
			printErrorMessage();
		}
	}
}

fn printMenu(){
	println!("{}. Solve a problem", Selections::SOLVE as i32);
	println!("{}. Print a problem description", Selections::DESCRIPTION as i32);
	println!("{}. List valid problem numbers", Selections::LIST as i32);
	println!("{}. Benchmark", Selections::BENCHMARK as i32);
	println!("{}. Exit\n", Selections::EXIT as i32);
}

fn getMenuSelection() -> Selections{
	//Setup reading from the console
	let mut selectionStr = String::new();

	std::io::stdin().read_line(&mut selectionStr).ok();
	//Convert the read to an int
	let mut selection = selectionStr.trim().parse::<i32>().unwrap();
	while(!isValidMenu(selection)){
		println!("That is an invalid option!");
		printMenu();
		std::io::stdin().read_line(&mut selectionStr).ok();
		selection = selectionStr.trim().parse::<i32>().unwrap();
	}
	return getSelection(selection);
}
fn isValidMenu(selection: i32) -> bool{
	if((selection > 0) && (selection < Selections::SIZE as i32)){
		return true;
	}
	else{
		return false;
	}
}
fn getSelection(selection: i32) -> Selections{
	if(selection == Selections::SOLVE as i32){
		return Selections::SOLVE;
	}
	else if(selection == Selections::DESCRIPTION as i32){
		return Selections::DESCRIPTION;
	}
	else if(selection == Selections::LIST as i32){
		return Selections::LIST;
	}
	else if(selection == Selections::BENCHMARK as i32){
		return Selections::BENCHMARK;
	}
	else if(selection == Selections::EXIT as i32){
		return Selections::EXIT;
	}
	else{
		return Selections::EMPTY;
	}
}
fn printErrorMessage(){
	println!("That is an invalid selection!");
}
fn solveMenu(){
	let problemNumber = Problems::getProblemNumber();
	println!("\n\n");
	//This selection solves all problems in order
	if(problemNumber == 0){
		//Solve to every valid problem number
		for problemLocation in 1..Problems::problemNumbers.len() as u32{
			//Solve the problems
			print!("{}. ", Problems::problemNumbers[problemLocation as usize]);
			let results = Problems::solveProblem(Problems::problemNumbers[problemLocation as usize], true, true);
			println!("{}\n\n", results);
		}
	}
	//This is if a single problem number was chosen
	else{
		//Solve the problem
		let results = Problems::solveProblem(problemNumber, true, true);
		println!("{}", results);
	}
	println!("\n\n");
}
fn descriptionMenu(){
	//Give some extra space to print the description
	println!("\n\n");

	//Get the problem number
	let problemNumber = Problems::getProblemNumber();
	let mut results = Problems::Answer::Answer::new("".to_string(), "".to_string(), 0);

	//If the problem number is 0 then print out all the descriptions
	if(problemNumber == 0){
		//Print decription for every valid problem number
		for problemLocation in 1..Problems::problemNumbers.len(){
			//Print the problem's description
			print!("{}. ", Problems::problemNumbers[problemLocation]);
			results = Problems::solveProblem(Problems::problemNumbers[problemLocation], true, false);
		}
		println!("{}", results);
	}
	//Otherwise print out a single problem's description
	else{
		results = Problems::solveProblem(problemNumber, true, false);
		println!("{}", results);
	}
	println!("\n\n");
}
fn benchmarkMenu(){
	benchmarkPrintMenu();
	let selection = benchmarkGetMenuSelection();

	if(selection == BenchmarkOptions::runSpecific){
		benchmarkRunSpecific();
	}
	else if(selection == BenchmarkOptions::runAllShort){
		benchmarkRunAllShort();
	}
	else if(selection == BenchmarkOptions::runAll){
		benchmarkRunAll();
	}
	else if(selection == BenchmarkOptions::exit){
	}
	else if(selection == BenchmarkOptions::empty){
	}
	else{
		printErrorMessage();
	}
}
fn benchmarkPrintMenu(){
	println!("{}. Run a specific problem", BenchmarkOptions::runSpecific as i32);
	println!("{}. Run all problems that have a reasonably short run time", BenchmarkOptions::runAllShort as i32);
	println!("{}. Run all problems", BenchmarkOptions::runAll as i32);
	println!("{}. Exit the menu\n", BenchmarkOptions::exit as i32);
}
fn benchmarkGetMenuSelection() -> BenchmarkOptions{
	//Setup reading from the console
	let mut selectionStr = String::new();

	std::io::stdin().read_line(&mut selectionStr).ok();
	//Convert the read to an int
	let mut selection = selectionStr.trim().parse::<i32>().unwrap();
	while(!isValidMenu(selection)){
		println!("That is an invalid option!");
		printMenu();
		std::io::stdin().read_line(&mut selectionStr).ok();
		selection = selectionStr.trim().parse::<i32>().unwrap();
	}
	return benchmarkGetSelection(selection);
}
fn benchmarkGetSelection(selection: i32) -> BenchmarkOptions{
	if(selection == BenchmarkOptions::runSpecific as i32){
		return BenchmarkOptions::runSpecific;
	}
	else if(selection == BenchmarkOptions::runAllShort as i32){
		return BenchmarkOptions::runAllShort;
	}
	else if(selection == BenchmarkOptions::runAll as i32){
		return BenchmarkOptions::runAll;
	}
	else if(selection == BenchmarkOptions::exit as i32){
		return BenchmarkOptions::exit;
	}
	else{
		return BenchmarkOptions::size;
	}
}
fn benchmarkRunSpecific(){
	//Ask which problem the user wants to run
	let problemNumber = Problems::getProblemNumber();
	//Ask how many times to run the problem
	let timesToRun = getNumberOfTimesToRun();

	//Print the problem's description
	println!("{}. ", problemNumber);
	Problems::solveProblem(problemNumber, true, false);
	//Run the problem the specific number of times
	let mut totalTime = 0;
	let results = benchmarkSolveProblem(problemNumber, timesToRun, &mut totalTime);

	//Print the results
	println!("{}", getBenchmarkResults(totalTime, timesToRun, results));
}
fn benchmarkRunAllShort(){
	//Ask how many times to run the problem
	let timesToRun = getNumberOfTimesToRun();

	//Run all problems that are not too long
	for problemLocation in 1..Problems::problemNumbers.len() as u32{
		//Skip any problem that is too long
		let problemNumber = Problems::problemNumbers[problemLocation as usize];
		if(Problems::tooLong.contains(&problemNumber)){
			continue;
		}
		//Solve the problems
		println!("{}. ", problemNumber);
		Problems::solveProblem(problemNumber, true, false);
		let mut totalTime = 0;
		let results = benchmarkSolveProblem(problemNumber, timesToRun, &mut totalTime);
		println!("\n\n");

		//Print the results
		println!("{}", getBenchmarkResults(totalTime, timesToRun, results));
	}
}
fn benchmarkRunAll(){
	//Ask how many times to run the problem
	let timesToRun = getNumberOfTimesToRun();

	//Run all problems
	for problemLocation in 1..Problems::problemNumbers.len() as u32{
		//Get the problem number
		let problemNumber = Problems::problemNumbers[problemLocation as usize];
		//Solve the problems
		println!("{}. ", problemNumber);
		Problems::solveProblem(problemNumber, true, false);
		let mut totalTime = 0;
		let results = benchmarkSolveProblem(problemNumber, timesToRun, &mut totalTime);
		println!("\n\n");

		//Print the results
		println!("{}", getBenchmarkResults(totalTime, timesToRun, results));
	}
}
fn getNumberOfTimesToRun() -> u32{
	print!("How many times do you want to run this problem? ");
	std::io::stdout().flush().unwrap();
	let mut numOfTimesToRunString = String::new();
	std::io::stdin().read_line(&mut numOfTimesToRunString).ok();
	//Convert to an int
	let mut numOfTimesToRun = numOfTimesToRunString.trim().parse::<u32>().unwrap();

	while(numOfTimesToRun < 1){
		println!("That is an invalid number!");
		println!("How many times to you want to run this problem? ");
		std::io::stdin().read_line(&mut numOfTimesToRunString).ok();
		//Convert to an int
		numOfTimesToRun = numOfTimesToRunString.trim().parse::<u32>().unwrap();
	}
	return numOfTimesToRun;
}
fn benchmarkSolveProblem(problemNumber: u32, timesToRun: u32, totalTime: &mut u128) -> String{
	let mut answer = Problems::Answer::Answer::new("".to_string(), "".to_string(), 0);
	*totalTime = 0;
	print!("Solving");
	for _ in 0..timesToRun{
		print!(".");
		std::io::stdout().flush().unwrap();
		//Solve the problem
		answer = Problems::solveProblem(problemNumber, false, true);
		//Get the time data
		*totalTime += &answer.getNano();
	}
	let results = format!("{}", answer.getResult());
	return results.to_string();
}
fn getBenchmarkResults(totalTime: u128, timesRun: u32, results: String) -> String{
	//Calculate the average run time of the problem
	let averageTime = (totalTime as f64) / (timesRun as f64);
	let timeResults = myClasses::Stopwatch::Stopwatch::getStr(averageTime);
	//Tally the results
	let benchmarkResult = format!("\n\n{}\nIt took an average of {} to run this problem through {} iterations\n\n", results, timeResults, timesRun);
	return benchmarkResult;
}
