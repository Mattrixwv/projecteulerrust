//ProjectEulerRust/src/Problems.rs
//Matthew Ellison
// Created: 06-11-20
//Modified: 06-17-20
//This file just forwards all the problem modules
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/RustClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


pub mod Answer;
pub mod Problem1;
pub mod Problem2;
pub mod Problem3;
pub mod Problem4;
pub mod Problem5;
pub mod Problem6;
pub mod Problem7;
pub mod Problem8;
pub mod Problem9;
pub mod Problem10;
pub mod Problem11;
pub mod Problem12;
pub mod Problem13;
pub mod Problem14;
pub mod Problem15;
pub mod Problem16;
pub mod Problem17;
pub mod Problem18;
pub mod Problem19;
pub mod Problem20;
pub mod Problem21;
pub mod Problem22;
pub mod Problem23;
pub mod Problem24;
pub mod Problem25;
pub mod Problem26;
pub mod Problem27;
pub mod Problem28;
pub mod Problem29;
pub mod Problem30;
pub mod Problem31;
pub mod Problem32;
pub mod Problem33;
pub mod Problem34;
pub mod Problem35;
pub mod Problem67;


pub static problemNumbers: [u32; 37] = [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
									10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
									20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
									30, 31, 32, 33, 34, 35, 67];
pub static tooLong: [u32; 8] = [3, 5, 15, 23, 24, 25, 27, 35];

pub fn solveProblem(problemNumber: u32, description: bool, solve: bool) -> Answer::Answer{
	let mut answer = Answer::Answer::new("".to_string(), "".to_string(), 0);
	//This is ever used, it's just to suppress the warning about getTime not being used in Answer
	if(problemNumber == 0){
		println!("In 0 {}", answer.getTime());
	}
	if(problemNumber == 1){
		if(description){
			println!("{}", Problem1::getDescription());
		}
		if(solve){
			answer = Problem1::solve();
		}
	}
	else if(problemNumber == 2){
		if(description){
			println!("{}", Problem2::getDescription());
		}
		if(solve){
			answer = Problem2::solve();
		}
	}
	else if(problemNumber == 3){
		if(description){
			println!("{}", Problem3::getDescription());
		}
		if(solve){
			answer = Problem3::solve();
		}
	}
	else if(problemNumber == 4){
		if(description){
			println!("{}", Problem4::getDescription());
		}
		if(solve){
			answer = Problem4::solve();
		}
	}
	else if(problemNumber == 5){
		if(description){
			println!("{}", Problem5::getDescription());
		}
		if(solve){
			answer = Problem5::solve();
		}
	}
	else if(problemNumber == 6){
		if(description){
			println!("{}", Problem6::getDescription());
		}
		if(solve){
			answer = Problem6::solve();
		}
	}
	else if(problemNumber == 7){
		if(description){
			println!("{}", Problem7::getDescription());
		}
		if(solve){
			answer = Problem7::solve();
		}
	}
	else if(problemNumber == 8){
		if(description){
			println!("{}", Problem8::getDescription());
		}
		if(solve){
			answer = Problem8::solve();
		}
	}
	else if(problemNumber == 9){
		if(description){
			println!("{}", Problem9::getDescription());
		}
		if(solve){
			answer = Problem9::solve();
		}
	}
	else if(problemNumber == 10){
		if(description){
			println!("{}", Problem10::getDescription());
		}
		if(solve){
			answer = Problem10::solve();
		}
	}
	else if(problemNumber == 11){
		if(description){
			println!("{}", Problem11::getDescription());
		}
		if(solve){
			answer = Problem11::solve();
		}
	}
	else if(problemNumber == 12){
		if(description){
			println!("{}", Problem12::getDescription());
		}
		if(solve){
			answer = Problem12::solve();
		}
	}
	else if(problemNumber == 13){
		if(description){
			println!("{}", Problem13::getDescription());
		}
		if(solve){
			answer = Problem13::solve();
		}
	}
	else if(problemNumber == 14){
		if(description){
			println!("{}", Problem14::getDescription());
		}
		if(solve){
			answer = Problem14::solve();
		}
	}
	else if(problemNumber == 15){
		if(description){
			println!("{}", Problem15::getDescription());
		}
		if(solve){
			answer = Problem15::solve();
		}
	}
	else if(problemNumber == 16){
		if(description){
			println!("{}", Problem16::getDescription());
		}
		if(solve){
			answer = Problem16::solve();
		}
	}
	else if(problemNumber == 17){
		if(description){
			println!("{}", Problem17::getDescription());
		}
		if(solve){
			answer = Problem17::solve();
		}
	}
	else if(problemNumber == 18){
		if(description){
			println!("{}", Problem18::getDescription());
		}
		if(solve){
			answer = Problem18::solve();
		}
	}
	else if(problemNumber == 19){
		if(description){
			println!("{}", Problem19::getDescription());
		}
		if(solve){
			answer = Problem19::solve();
		}
	}
	else if(problemNumber == 20){
		if(description){
			println!("{}", Problem20::getDescription());
		}
		if(solve){
			answer = Problem20::solve();
		}
	}
	else if(problemNumber == 21){
		if(description){
			println!("{}", Problem21::getDescription());
		}
		if(solve){
			answer = Problem21::solve();
		}
	}
	else if(problemNumber == 22){
		if(description){
			println!("{}", Problem22::getDescription());
		}
		if(solve){
			answer = Problem22::solve();
		}
	}
	else if(problemNumber == 23){
		if(description){
			println!("{}", Problem23::getDescription());
		}
		if(solve){
			answer = Problem23::solve();
		}
	}
	else if(problemNumber == 24){
		if(description){
			println!("{}", Problem24::getDescription());
		}
		if(solve){
			answer = Problem24::solve();
		}
	}
	else if(problemNumber == 25){
		if(description){
			println!("{}", Problem25::getDescription());
		}
		if(solve){
			answer = Problem25::solve();
		}
	}
	else if(problemNumber == 26){
		if(description){
			println!("{}", Problem26::getDescription());
		}
		if(solve){
			answer = Problem26::solve();
		}
	}
	else if(problemNumber == 27){
		if(description){
			println!("{}", Problem27::getDescription());
		}
		if(solve){
			answer = Problem27::solve();
		}
	}
	else if(problemNumber == 28){
		if(description){
			println!("{}", Problem28::getDescription());
		}
		if(solve){
			answer = Problem28::solve();
		}
	}
	else if(problemNumber == 29){
		if(description){
			println!("{}", Problem29::getDescription());
		}
		if(solve){
			answer = Problem29::solve();
		}
	}
	else if(problemNumber == 30){
		if(description){
			println!("{}", Problem30::getDescription());
		}
		if(solve){
			answer = Problem30::solve();
		}
	}
	else if(problemNumber == 31){
		if(description){
			println!("{}", Problem31::getDescription());
		}
		if(solve){
			answer = Problem31::solve();
		}
	}
	else if(problemNumber == 32){
		if(description){
			println!("{}", Problem32::getDescription());
		}
		if(solve){
			answer = Problem32::solve();
		}
	}
	else if(problemNumber == 33){
		if(description){
			println!("{}", Problem33::getDescription());
		}
		if(solve){
			answer = Problem33::solve();
		}
	}
	else if(problemNumber == 34){
		if(description){
			println!("{}", Problem34::getDescription());
		}
		if(solve){
			answer = Problem34::solve();
		}
	}
	else if(problemNumber == 35){
		if(description){
			println!("{}", Problem35::getDescription());
		}
		if(solve){
			answer = Problem35::solve();
		}
	}
	else if(problemNumber == 67){
		if(description){
			println!("{}", Problem67::getDescription());
		}
		if(solve){
			answer = Problem67::solve();
		}
	}
	return answer;
}

pub fn getProblemNumber() -> u32{
	println!("Enter a problem number: ");
	//Setup reading from the console
	let mut problemNumberStr = String::new();
	std::io::stdin().read_line(&mut problemNumberStr).ok();
	//Convert the read to an int
	let mut problemNumber = problemNumberStr.trim().parse::<u32>().unwrap();

	//Loop through all valid problem numbers and see if the one entered matches it
	while(!problemNumbers.contains(&problemNumber)){
		println!("That is an invalid problem number!\nEnter a problem number: ");
		std::io::stdin().read_line(&mut problemNumberStr).ok();
		problemNumber = problemNumberStr.trim().parse::<u32>().unwrap();
	}
	return problemNumber;
}
pub fn listProblems(){
	print!("Problems: {}", problemNumbers[1]);
	for problemNumber in 2..problemNumbers.len(){
		println!(", {}", problemNumbers[problemNumber])
	}
	println!();
}